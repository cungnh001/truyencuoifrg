package com.cungnh.truyencuoifrg;

import android.app.Application;

public class App extends Application {
    private StorageCommon storage ;
    private static App instance ;


    @Override
    public void onCreate() {
        super.onCreate();
        storage = new StorageCommon();
        instance = this ;
    }

    public static App getInstance() {
        return instance;
    }

    public StorageCommon getStorage() {
        return storage;
    }
}
