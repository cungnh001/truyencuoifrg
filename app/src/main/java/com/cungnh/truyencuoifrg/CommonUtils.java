package com.cungnh.truyencuoifrg;

import android.content.SharedPreferences;
import android.util.Log;

import com.cungnh.truyencuoifrg.entity.StoryEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class CommonUtils {
    private static final String STORY_PATH = "dangian.txt";
    private static final String TAG = CommonUtils.class.getName();
    private static final String PREF_NAME = "prefname";

    public static final String PREF_SAVE = "save_file";
    public static final String KEY_SAVE = "key_save";
    public static final String KEY_POSITION = "key_position";
    public static final String KEY_TOPIC = "KEY_TOPIC";
    private static CommonUtils instance;

    private static  String[] NAME_STORY = new String[]{"congai.txt", "congso.txt","connit.txt", "contrai.txt", "cuchai.txt",
            "cuoi18+.txt", "dangian.txt", "giadinh.txt", "giaothong.txt"};

    private CommonUtils() {

    }

    public static CommonUtils getInstance() {
        if (instance == null) {
            instance = new CommonUtils();
        }
        return instance;
    }

    public List<StoryEntity> getStory(int pos  ) {
        List<StoryEntity> listStory = new ArrayList<>();
        try {
            String storyPath = "data/" + NAME_STORY[pos];
            InputStream inputStream = App.getInstance().getAssets().open(storyPath);
            BufferedReader buf = new BufferedReader(new InputStreamReader(inputStream));
            String name = null, content = "";
            String line = buf.readLine();
            while (line != null) {
                if (name == null) {
                    name = line;
                } else if (line.contains("','0');")) {
                    StoryEntity story = new StoryEntity(name, content, false);
                    listStory.add(story);
                    name = null;
                    content = "";
                } else {
                    content += line + "\n";

                }
                line = buf.readLine();
            }
            buf.close();
            Log.i(TAG, "getStory... done " + listStory.size());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return listStory;
    }

    public void savePositionStory(int positionStory, int topic) {
        SharedPreferences preferences = App.getInstance().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(KEY_POSITION, positionStory).apply();
        editor.putInt(KEY_TOPIC, topic).apply();

    }

    public int getPositionTopic() {
        SharedPreferences preferences = App.getInstance().getSharedPreferences(PREF_NAME, MODE_PRIVATE);

        return preferences.getInt(KEY_TOPIC, 0);
    }

    public int getPositionStory() {
        SharedPreferences preferences = App.getInstance().getSharedPreferences(PREF_NAME, MODE_PRIVATE);

        return preferences.getInt(KEY_POSITION, 0);
    }





}
