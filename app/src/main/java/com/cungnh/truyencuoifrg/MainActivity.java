package com.cungnh.truyencuoifrg;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.cungnh.truyencuoifrg.event.OnMainActCallBack;
import com.cungnh.truyencuoifrg.fragment.DetailTopicFrg;
import com.cungnh.truyencuoifrg.fragment.ListFavoriteFrg;
import com.cungnh.truyencuoifrg.fragment.ListTopicFrg;
import com.cungnh.truyencuoifrg.fragment.StoryContentFrg;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMainActCallBack {

    private Button btTopic, btFavorite;
    private ListTopicFrg listTopicFrg;
    private DetailTopicFrg detailTopicFrg;
    private StoryContentFrg storyContentFrg;
    private ListFavoriteFrg listFavoriteFrg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
//        btTopic = findViewById(R.id.bt_topic);
//        btFavorite = findViewById(R.id.bt_favorite);
//
//        btTopic.setOnClickListener(this);
//        btFavorite.setOnClickListener(this);
        showListTopicFragment();
    }

    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.bt_topic) {
//            changeTap(btTopic, btFavorite);
//            showListTopicFragment();
//        } else if (v.getId() == R.id.bt_favorite) {
//            changeTap(btFavorite, btTopic);
////            showFragment(DetailContactFrg.TAG);
//            showListFavoriteFrg();
//        }
    }

    private void showListTopicFragment() {
        if (listTopicFrg == null) {
            listTopicFrg = new ListTopicFrg();
        }

        listTopicFrg.setCallBack(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.ln_main, listTopicFrg, ListTopicFrg.TAG).commit();

    }

    private void changeTap(Button selectTap, Button deselectTap) {
        selectTap.setBackgroundResource(R.color.colorSelected);
        deselectTap.setBackgroundResource(R.color.colorGreen);
    }

//    public final StorageCommon getStorage() {
//        return App.getInstance().getStorage();
//    }

    @Override
    public void callBack(int topicPos) {
        App.getInstance().getStorage().setTopicPosition(topicPos);
        showDetailTopicFrg();
    }


    private void showDetailTopicFrg() {
        if (detailTopicFrg == null) {
            detailTopicFrg = new DetailTopicFrg();
        }
        detailTopicFrg.setCallBack(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.ln_main, detailTopicFrg, DetailTopicFrg.TAG).commit();

    }

    @Override
    public void callBack(String key, Object data) {
        if (key.equals(DetailTopicFrg.KEY_SHOW_CONTENT)) {
            App.getInstance().getStorage().setStoryPos((Integer) data);
            showStoryContentFrg();
        } else if (key.equals(StoryContentFrg.KEY_BACK_LIST)) {
            App.getInstance().getStorage().setTopicPosition((Integer) data);
            showDetailTopicFrg();
        } else if (key.equals(ListTopicFrg.KEY_LIST)) {
            showListFavoriteFrg();
        } else if (key.equals(ListFavoriteFrg.BACK_LIST)) {
            showListTopicFragment();
        }

    }

    private void showListFavoriteFrg() {
        if (listFavoriteFrg == null) {
            listFavoriteFrg = new ListFavoriteFrg();
        }
        listFavoriteFrg.setCallBack(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.ln_main, listFavoriteFrg, ListFavoriteFrg.TAG).commit();

    }

    private void showStoryContentFrg() {
        if (storyContentFrg == null) {
            storyContentFrg = new StoryContentFrg();
        }
        storyContentFrg.setCallBack(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.ln_main, storyContentFrg, DetailTopicFrg.TAG).commit();

    }

    @Override
    public void callBack() {
        showListTopicFragment();
    }
}
