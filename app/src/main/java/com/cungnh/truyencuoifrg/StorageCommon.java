package com.cungnh.truyencuoifrg;

import com.cungnh.truyencuoifrg.entity.StoryEntity;

public class StorageCommon {
    private StoryEntity storyEntity;
    private int topicPosition, storyPos;

    public StoryEntity getStoryEntity() {
        return storyEntity;
    }

    public void setStoryEntity(StoryEntity storyEntity) {
        this.storyEntity = storyEntity;
    }

    public int getTopicPosition() {
        return topicPosition;
    }

    public void setTopicPosition(int topicPosition) {
        this.topicPosition = topicPosition;
    }

    public void setStoryPos(int storyPos) {
        this.storyPos = storyPos;
    }

    public int getStoryPos() {
        return storyPos;
    }
}
