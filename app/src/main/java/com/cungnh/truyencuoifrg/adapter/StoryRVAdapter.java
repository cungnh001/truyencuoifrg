package com.cungnh.truyencuoifrg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cungnh.truyencuoifrg.R;
import com.cungnh.truyencuoifrg.entity.StoryEntity;

import java.util.List;

public class StoryRVAdapter extends RecyclerView.Adapter<StoryRVAdapter.StoryHolder> {

    private List<StoryEntity> listData;
    private Context context;
    private OnItemCallBackRV callBackRV;

    public StoryRVAdapter(OnItemCallBackRV callBackRV, List<StoryEntity> listData, Context context) {
        this.listData = listData;
        this.context = context;
        this.callBackRV = callBackRV;
    }

    @NonNull
    @Override
    public StoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.favorite_story_name, null);
        return new StoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StoryHolder holder, int position) {
        StoryEntity storyEntity = listData.get(position);
        holder.tvName.setText(storyEntity.getName());
        holder.tvName.setTag(storyEntity);


    }

    @Override
    public int getItemCount() {
        return listData.size();
    }


    class StoryHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        ImageView ivRemove;

        public StoryHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_favorite_story_name);
            ivRemove = itemView.findViewById(R.id.iv_remove);

//            tvName.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callBackRV.clickItemRV((StoryEntity) tvName.getTag());
//                }
//            });

            tvName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBackRV.toStoryContent((StoryEntity) tvName.getTag());
                }
            });

            ivRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBackRV.clickItemReMove((StoryEntity) tvName.getTag());
                }
            });

        }
    }

    public interface OnItemCallBackRV {
        void clickItemReMove(StoryEntity story);

        void toStoryContent(StoryEntity story);
    }
}
