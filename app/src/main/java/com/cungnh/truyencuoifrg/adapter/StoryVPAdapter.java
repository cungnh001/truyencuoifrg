package com.cungnh.truyencuoifrg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.cungnh.truyencuoifrg.R;
import com.cungnh.truyencuoifrg.entity.StoryEntity;
import com.cungnh.truyencuoifrg.fragment.StoryContentFrg;

import java.util.List;

public class StoryVPAdapter extends PagerAdapter {
    private static final int LEVEL_UNLIKE = 0;
    private static final int LEVEL_LIKE = 1;
    private Context context;
    private List<StoryEntity> listData;
    private OnItemCallBack callBack;

    public StoryVPAdapter(OnItemCallBack callBack, Context context, List<StoryEntity> listData) {
        this.context = context;
        this.listData = listData;
        this.callBack = callBack;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.story_content, null);
        TextView tvName = itemView.findViewById(R.id.tv_name);
        TextView tvContent = itemView.findViewById(R.id.tv_story_content);
        final ImageView ivFavorite = itemView.findViewById(R.id.iv_favorite);

        final StoryEntity storyEntity = listData.get(position);
        tvName.setText(storyEntity.getName());
        tvContent.setText(storyEntity.getContent());
        itemView.setTag(storyEntity);

//        View.OnClickListener clickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                storyEntity.setFavorite(true);
//                ivFavorite.setImageLevel(ivFavorite.getDrawable().getLevel() == LEVEL_UNLIKE ? LEVEL_LIKE : LEVEL_UNLIKE);
//                callBack.clickItem((StoryEntity) itemView.getTag());
//            }
//        };

        ivFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storyEntity.setFavorite(true);
//                ivFavorite.setImageLevel(ivFavorite.getDrawable().getLevel() == LEVEL_UNLIKE ? LEVEL_LIKE : LEVEL_UNLIKE);
//                callBack.clickItem((StoryEntity) v.getTag());
                callBack.clickItem(storyEntity);
            }
        });
//        tvContent.setOnClickListener(clickListener);
//        tvName.setOnClickListener(clickListener);

        container.addView(itemView);


        return itemView;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup viewPager, int position, @NonNull Object object) {
        viewPager.removeView((View) object);

    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    public interface OnItemCallBack {
        void clickItem(StoryEntity storyEntity);
    }
}
