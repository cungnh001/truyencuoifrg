package com.cungnh.truyencuoifrg.entity;

import android.graphics.Bitmap;

public class TopicEntity {
    private String topicName ;
    private Bitmap topicImg ;

    public TopicEntity(String topicName, Bitmap topicImg) {
        this.topicName = topicName;
        this.topicImg = topicImg;
    }

    public String getTopicName() {
        return topicName;
    }

    public Bitmap getTopicImg() {
        return topicImg;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void setTopicImg(Bitmap topicImg) {
        this.topicImg = topicImg;
    }
}
