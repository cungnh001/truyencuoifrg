package com.cungnh.truyencuoifrg.event;

public interface OnMainActCallBack extends OnActionCallBack {
    void callBack(int topicPos );
    void callBack();
}
