package com.cungnh.truyencuoifrg.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cungnh.truyencuoifrg.event.OnActionCallBack;

public abstract class BaseFragment <C  extends OnActionCallBack> extends Fragment implements  View.OnClickListener {
    protected Context mContext ;
    protected View rootView;
    protected C callBack;
    @Override
    public final void onAttach(@NonNull Context context) {

        super.onAttach(context);
        mContext = context;
    }

    public void setCallBack(C event){
        callBack = event;
    }
    @Nullable
    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       rootView = inflater.inflate(getLayoutId(), container, false);
       initView();
        return rootView;
    }

    protected abstract int getLayoutId() ;

    protected abstract void initView() ;

    @Override
    public void onClick(View v) {

    }


    protected final   <T extends View> T findViewById(int id, View.OnClickListener event) {
        T v = rootView.findViewById(id);
        if (v != null && event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    protected final   <T extends View> T findViewById(int id) {
        return findViewById(id,null);
    }


}
