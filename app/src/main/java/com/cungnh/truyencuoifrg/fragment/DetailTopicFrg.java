package com.cungnh.truyencuoifrg.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cungnh.truyencuoifrg.App;
import com.cungnh.truyencuoifrg.CommonUtils;
import com.cungnh.truyencuoifrg.R;
import com.cungnh.truyencuoifrg.entity.StoryEntity;
import com.cungnh.truyencuoifrg.event.OnMainActCallBack;

import java.util.ArrayList;
import java.util.List;


public class DetailTopicFrg extends BaseFragment<OnMainActCallBack> {
    public static final String TAG = DetailTopicFrg.class.getName();
    public static final String KEY_SHOW_CONTENT = "KEY_SHOW_CONTENT";
    private List<StoryEntity> listStory;
    private LinearLayout lnListStory;
    private TextView tvTopic;
    private Button btTopic, btFavorite;


    @Override
    protected int getLayoutId() {
        return R.layout.frg_detail_topic;
    }


    @Override
    protected void initView() {
         btTopic = findViewById(R.id.bt_topic);
         btFavorite = findViewById(R.id.bt_favorite);;
        findViewById(R.id.bt_topic, this);
        findViewById(R.id.bt_favorite,this);

        lnListStory = findViewById(R.id.ln_list_story);
        lnListStory.removeAllViews();
        findViewById(R.id.bt_back_topic, this);

        int topicPos = App.getInstance().getStorage().getTopicPosition();

        tvTopic = findViewById(R.id.tv_topic_actionbar);
        tvTopic.setText(ListTopicFrg.NAME_TOPIC[topicPos]);

        initData(topicPos);

    }

    private void initData(int topicPos) {
        listStory = new ArrayList<>();
        listStory = CommonUtils.getInstance().getStory(topicPos);

        for (int i = 0; i < listStory.size(); i++) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.story_name, null);

            TextView tvStoryName = itemView.findViewById(R.id.tv_story_name);
            tvStoryName.setText(listStory.get(i).getName());

            lnListStory.addView(itemView);
            itemView.setTag(i);
            final int finalI = i;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.callBack(KEY_SHOW_CONTENT, finalI);
                }
            });

        }
    }


//    public void setContact(ContactEntity contact) {
//        mContact = contact;
//    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_back_topic) {
            callBack.callBack();
        }
        else if (v.getId() == R.id.bt_topic) {
            changeTap(btTopic, btFavorite);
//            showListTopicFragment();
        } else if (v.getId() == R.id.bt_favorite) {
            changeTap(btFavorite, btTopic);
//            showFragment(DetailContactFrg.TAG);
        }
    }

    private void changeTap(Button selectTap, Button deselectTap) {
        selectTap.setBackgroundResource(R.color.colorSelected);
        deselectTap.setBackgroundResource(R.color.colorGreen);
    }
}
