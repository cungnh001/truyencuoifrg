package com.cungnh.truyencuoifrg.fragment;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cungnh.truyencuoifrg.R;
import com.cungnh.truyencuoifrg.adapter.StoryRVAdapter;
import com.cungnh.truyencuoifrg.entity.StoryEntity;

public class ListFavoriteFrg extends BaseFragment implements StoryRVAdapter.OnItemCallBackRV {


    public static final String BACK_LIST = "BACK_LIST";
    public static String TAG = ListFavoriteFrg.class.getName();
    private RecyclerView rvListFavorite;
    private StoryRVAdapter storyRVAdapter;
    private TextView tvTopicName;

    @Override
    protected int getLayoutId() {
        return R.layout.frg_list_favorite_story;
    }

    @Override
    protected void initView() {
        rvListFavorite = (RecyclerView) findViewById(R.id.rv_favorite);
        tvTopicName = (TextView) findViewById(R.id.tv_topic_actionbar);

        tvTopicName.setText("Yeu thich");
        findViewById(R.id.bt_back_topic, this);

        storyRVAdapter = new StoryRVAdapter(this, StoryContentFrg.listFavorite, mContext);
        rvListFavorite.setLayoutManager(new LinearLayoutManager(mContext));

        rvListFavorite.setAdapter(storyRVAdapter);
        if( StoryContentFrg.listFavorite.size() == 0){
            Toast.makeText(mContext, "ListFavoriteStory is empty", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void clickItemReMove(StoryEntity story) {
        StoryContentFrg.listFavorite.remove(story);
        storyRVAdapter.notifyDataSetChanged();
    }

    @Override
    public void toStoryContent(StoryEntity story) {

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.bt_back_topic){
            callBack.callBack(BACK_LIST, null);
        }
    }
}
