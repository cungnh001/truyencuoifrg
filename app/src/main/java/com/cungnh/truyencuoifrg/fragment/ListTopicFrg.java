package com.cungnh.truyencuoifrg.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cungnh.truyencuoifrg.R;
import com.cungnh.truyencuoifrg.entity.TopicEntity;
import com.cungnh.truyencuoifrg.event.OnMainActCallBack;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ListTopicFrg extends BaseFragment<OnMainActCallBack> {
    public static final String TAG = ListTopicFrg.class.getName();
    public static final String KEY_LIST = "KEY_LIST";


    public static final String[] NAME_TOPIC = new String[]{"Con gái", "Công sở",
            "Con nít", "Con trai",
            "Cực hài", "Cười 18", "Dân gian", "Gia đình", "Giao thông"};
    public static final String[] IMG_TOPIC = new String[]{"congai", "congso",
            "connit", "contrai",
            "cuchai", "cuoi18", "dangian", "giadinh", "giaothong"};

    private List<TopicEntity> listTopic;

    private LinearLayout lnListTopic;
    private int selectedIndex = -1;

    @Override
    protected int getLayoutId() {
        return R.layout.frg_list_topic;
    }


    @Override
    protected void initView() {
        initData();
        lnListTopic = findViewById(R.id.ln_list_topic);
        lnListTopic.removeAllViews();
        findViewById(R.id.bt_favorite, this);


        for (int i = 0; i < listTopic.size(); i++) {
            View itemView = LayoutInflater.from(mContext).inflate(R.layout.story_topic, null);

            TextView tvName = itemView.findViewById(R.id.tv_story_topic);
            ImageView ivImage = itemView.findViewById(R.id.iv_story_topic);
            tvName.setText(listTopic.get(i).getTopicName());
            ivImage.setImageBitmap(listTopic.get(i).getTopicImg());

            lnListTopic.addView(itemView);
            itemView.setTag(i);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    App.getInstance().getStorage().setStoryPos((Integer) v.getTag());
                    callBack.callBack((Integer) v.getTag());
                }
            });


        }

        if (selectedIndex != -1) {
            lnListTopic.getChildAt(selectedIndex).setBackgroundResource(R.color.colorAccent);
        }

    }

    private void initData() {
        listTopic = new ArrayList<>();
        for (int i = 0; i < NAME_TOPIC.length; i++) {
            try {
                InputStream in = mContext.getAssets().open("icon/vn/" + IMG_TOPIC[i] + ".png");
                Bitmap img = BitmapFactory.decodeStream(in);
                listTopic.add(new TopicEntity(NAME_TOPIC[i], img));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onClick(View v) {
        for (int i = 0; i < lnListTopic.getChildCount(); i++) {
            lnListTopic.getChildAt(i).setBackgroundResource(R.color.colorWhite);
        }

        v.setBackgroundResource(R.color.colorAccent);
//        callBack.callBack((Integer) v.getTag());
//        selectedIndex = listContact.indexOf((ContactEntity) v.getTag());
        if (v.getId() == R.id.bt_favorite) {
            callBack.callBack(KEY_LIST, null);
        }

    }
}
