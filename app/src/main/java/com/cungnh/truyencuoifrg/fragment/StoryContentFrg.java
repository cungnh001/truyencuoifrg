package com.cungnh.truyencuoifrg.fragment;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.cungnh.truyencuoifrg.App;
import com.cungnh.truyencuoifrg.CommonUtils;
import com.cungnh.truyencuoifrg.R;
import com.cungnh.truyencuoifrg.adapter.StoryPageChangeAdapter;
import com.cungnh.truyencuoifrg.adapter.StoryVPAdapter;
import com.cungnh.truyencuoifrg.entity.StoryEntity;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class StoryContentFrg extends BaseFragment implements StoryVPAdapter.OnItemCallBack {
    public static final String KEY_BACK_LIST = "KEY_BACK_LIST";
    private ViewPager vpStory;
    private List<StoryEntity> listData = new ArrayList<>();
    public static List<StoryEntity> listFavorite = new ArrayList<>();
    private TextView tvPage, tvTopic;
    private StoryVPAdapter mAdapter;
    private int topicPos, storyPos;


    @Override
    protected int getLayoutId() {
        return R.layout.frg_story_content;
    }

    @Override
    protected void initView() {


        vpStory = (ViewPager) findViewById(R.id.vp_story);
        topicPos = App.getInstance().getStorage().getTopicPosition();
        storyPos = App.getInstance().getStorage().getStoryPos();

        Log.i("StoryPos", storyPos + "");
        listData = new ArrayList<>();
        listData = CommonUtils.getInstance().getStory(topicPos);

        findViewById(R.id.iv_back, this);

        tvPage = (TextView) findViewById(R.id.tv_page);
        tvTopic = (TextView) findViewById(R.id.tv_content_topic_actionbar);
        tvTopic.setText(ListTopicFrg.NAME_TOPIC[topicPos]);
        tvPage.setText(storyPos + 1 + "/" + listData.size());


        mAdapter = new StoryVPAdapter(this, mContext, listData);

        vpStory.addOnPageChangeListener(new StoryPageChangeAdapter() {
            @Override
            public void onPageSelected(int position) {
                tvPage.setText(MessageFormat.format("{0}/{1}", position + 1, listData.size()));
            }
        });

        vpStory.setAdapter(mAdapter);
        vpStory.setCurrentItem(storyPos);



//        findViewById(R.id.bt_favorite).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(mContext, "Favorite", Toast.LENGTH_SHORT).show();
//            }
//        });
    }


    @Override
    public void clickItem(StoryEntity storyEntity) {
        if (!listFavorite.contains(storyEntity)) {
            listFavorite.add(storyEntity);
        }
        Toast.makeText(mContext, "ListFavorite: " + listFavorite.size(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back) {
            callBack.callBack(KEY_BACK_LIST, topicPos);

        }
    }

}
